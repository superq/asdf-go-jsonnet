# asdf-go-jsonnet

As [asdf](https://github.com/asdf-vm/asdf) plugin for
[go-jsonnet](https://github.com/google/go-jsonnet).

## Installation

```
asdf plugin add go-jsonnet https://gitlab.com/craigfurman/asdf-go-jsonnet.git
```

This plugin installs binaries named `jsonnet` and `jsonnetfmt`, so it will
conflict in your `$PATH` with any other jsonnet installation.

Requires:

* go
* curl
* jq
